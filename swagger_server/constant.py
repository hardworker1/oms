OMS_AUTHORIZATION = "Bearer mi-oms"
MONGO_DB_NAME = "hippo"
COLLECTION_NAME = "mi-oms"
PUBLIC_HOST = "127.0.0.1"

# oms parameter
OMS_NAMESPACE = "namespace"
OMS_DATA_KEY = "dataKey"
OMS_DATA_VALUE = "dataValue"
OMS_CREATE_TIME = "createTime"
OMS_UPDATE_TIME = "updateTime"
