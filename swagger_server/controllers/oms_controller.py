import connexion
import six
from swagger_server.models.create_request import CreateRequest  # noqa: E501
from swagger_server.models.create_request_data import CreateRequestData
from swagger_server.models.create_response import CreateResponse  # noqa: E501
from swagger_server.models.delete_reponse import DeleteReponse  # noqa: E501
from swagger_server.models.retrieve_response import RetrieveResponse  # noqa: E501
from swagger_server.models.update_request import UpdateRequest  # noqa: E501
from swagger_server.models.update_response import UpdateResponse  # noqa: E501
from swagger_server import util
from swagger_server.constant import *
from swagger_server.controllers.oms_create_provider import CreateProvider
from swagger_server.controllers.oms_retrieve_provider import RetrieveProvider
from swagger_server.controllers.oms_delete_provider import DeleteProvider
from swagger_server.controllers.oms_update_provider import UpdateProvider

def create(create_request, Authorization=None,):  # noqa: E501
    """create

     # noqa: E501

    :param Authorization: 
    :type Authorization: str
    :param create_request:
    :type create_request: dict | bytes

    :rtype: CreateResponse
    """
    authorization = connexion.request.headers.get("Authorization")
    if authorization != OMS_AUTHORIZATION:
        raise Exception("The request has no authorization")
    if not connexion.request.is_json:
        raise Exception("request parameter invalid")
    create_request = CreateRequest.from_dict(connexion.request.get_json())  # noqa: E501
    oms_request_provider = CreateProvider(create_request)
    return oms_request_provider.create()


def delete(namespace, dataKey, Authorization=None):  # noqa: E501
    """delete
     # noqa: E501
    :param Authorization: 
    :type Authorization: str
    :param namespace: 
    :type namespace: str
    :param dataKey: 
    :type dataKey: str

    :rtype: DeleteReponse
    """
    authorization = connexion.request.headers.get("Authorization")
    if authorization != OMS_AUTHORIZATION:
        raise Exception("The request has no authorization")
    delete_request = DeleteProvider(namespace, dataKey)
    return delete_request.delete()


def retrieve(namespace, dataKey, pageFrom=0, pageSize=10, filedSort=None, requiredTotalSize=False, Authorization=None):  # noqa: E501
    """retrieve
     # noqa: E501
    :param Authorization: 
    :type Authorization: str
    :param namespace: 
    :type namespace: str
    :param dataKey: 
    :type dataKey: str
    :param pageFrom: 
    :type pageFrom: int
    :param pageSize: 
    :type pageSize: int
    :param filedSort: 
    :type filedSort: str
    :param requiredTotalSize: 
    :type requiredTotalSize: bool

    :rtype: RetrieveResponse
    """
    authorization = connexion.request.headers.get("Authorization")
    if authorization != OMS_AUTHORIZATION:
        raise Exception("The request has no authorization")
    oms_request_provider = RetrieveProvider(namespace, dataKey, pageFrom, pageSize, filedSort, requiredTotalSize)
    return oms_request_provider.retrieve()


def update(namespace, update_request, Authorization=None):  # noqa: E501
    """update
     # noqa: E501
    :param Authorization: 
    :type Authorization: str
    :param namespace: 
    :type namespace: str
    :param update_request: 
    :type update_request: dict | bytes

    :rtype: UpdateResponse
    """
    authorization = connexion.request.headers.get("Authorization")
    if authorization != OMS_AUTHORIZATION:
        raise Exception("The request has no authorization")
    if not connexion.request.is_json:
        raise Exception("The request has no json")
    request_data = UpdateRequest.from_dict(connexion.request.get_json())
    update_response = UpdateProvider(namespace, request_data)
    return update_response.update()
