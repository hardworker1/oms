from pymongo import MongoClient
from swagger_server.constant import *


class MongoDBClient:
    def __init__(self):
        self.mongo_client = MongoClient(PUBLIC_HOST)
        self.mongodb = self.mongo_client[MONGO_DB_NAME]
        self.collection = self.mongodb[COLLECTION_NAME]

    def insert(self, data):
        return self.collection.insert_one(data)

    def retrieve(self, data):
        return self.collection.find(data)

    def update(self, select_data, update_data):
        return self.collection.update_many(select_data, update_data)

    def delete(self, data):
        return self.collection.delete_many(data)