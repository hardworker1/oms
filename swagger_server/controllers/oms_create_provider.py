from swagger_server.controllers.mongo_db_client import MongoDBClient
import datetime
from swagger_server.constant import *


class CreateProvider:
    def __init__(self, create_request):
        self.client = MongoDBClient()
        self.create_request = create_request

    def create(self):
        create_body = self.build_parameter()
        try:
            item_id = self.execute_db_request(create_body)
        except:
            raise Exception("create data error")
        if item_id:
            return None

    def execute_db_request(self, create_body):
        is_existed = list(self.client.retrieve({
            OMS_NAMESPACE: create_body[OMS_NAMESPACE],
            OMS_DATA_KEY: create_body[OMS_DATA_KEY]
        }))
        if is_existed:
            raise Exception("create data error, because the data is existed")
        item_id = self.client.insert(create_body).inserted_id
        return item_id

    def build_parameter(self):
        create_body = {}
        now_time = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
        request_data = self.create_request.data
        create_body[OMS_DATA_KEY] = request_data.data_key
        create_body[OMS_DATA_VALUE] = request_data.data_value
        create_body[OMS_NAMESPACE] = request_data.namespace
        create_body[OMS_CREATE_TIME] = now_time
        create_body[OMS_UPDATE_TIME] = now_time
        return create_body
