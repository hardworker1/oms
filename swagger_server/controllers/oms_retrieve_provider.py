from swagger_server.constant import *
from swagger_server.controllers.mongo_db_client import MongoDBClient
import re
import pymongo
from swagger_server.models.retrieve_response import RetrieveResponse
from swagger_server.models.retrieve_response_data import RetrieveResponseData
from swagger_server.models.retrieve_response_data_item import RetrieveResponseDataItem


class RetrieveProvider:
    def __init__(self, namespace, data_key, page_from, page_size, filed_sort, required_total_size):
        self.client = MongoDBClient()
        self.namespace = namespace
        self.data_key = data_key
        self.page_from = page_from
        self.page_size = page_size
        self.filed_sort = filed_sort
        self.required_total_size = required_total_size

    def retrieve(self):
        query_body = self.build_parameter()
        item_cursor = self.execute_db_request(query_body)
        return self.build_response(item_cursor, query_body)

    def build_response(self, item_cursor, query_body):
        retrieve_response = RetrieveResponse()
        retrieve_response.api_version = "1.0"
        retrieve_response_data = RetrieveResponseData()
        if self.required_total_size:
            retrieve_response_data.total_size = self.get_total_size(query_body)
        retrieve_response_data_items = []
        for item in item_cursor:
            retrieve_response_data_item = RetrieveResponseDataItem()
            retrieve_response_data_item.create_time = item.get("createTime")
            retrieve_response_data_item.data_key = item.get("dataKey")
            retrieve_response_data_item.data_value = item.get("dataValue")
            retrieve_response_data_item.namespace = item.get("namespace")
            retrieve_response_data_item.update_time = item.get("updateTime")
            retrieve_response_data_item.id = str(item.get("_id"))
            retrieve_response_data_items.append(retrieve_response_data_item)
        retrieve_response_data.items = retrieve_response_data_items
        retrieve_response.data = retrieve_response_data
        return retrieve_response

    def get_total_size(self, query_body):
        return self.client.collection.find(query_body, projection={"_id": 1}).count()

    def execute_db_request(self, query_body):
        if self.filed_sort:
            filed_sort_data = self.get_field_sort()
            try:
                item_cursor = self.client.retrieve(query_body).limit(self.page_size)\
                    .skip(self.page_from).sort([filed_sort_data])
            except:
                raise Exception("retrieve data error")
        else:
            try:
                item_cursor = self.client.retrieve(query_body).limit(self.page_size).skip(self.page_from)
            except:
                raise Exception("retrieve data error")
        return item_cursor

    def build_parameter(self):
        query_body = {}
        query_body[OMS_NAMESPACE] = self.namespace
        if self.data_key.startswith("regex::") and self.data_key != "regex::":
            key_pattern = re.match("regex::(.*)", self.data_key).group(1)
            try:
                new_data_key = re.compile(key_pattern)
            except:
                raise Exception("Illegal data key")
            query_body[OMS_DATA_KEY] = new_data_key
        else:
            query_body[OMS_DATA_KEY] = self.data_key
        return query_body

    def get_field_sort(self):
        field_sort_data = re.match("(^[\+\-])(\w+)", self.filed_sort)
        field = field_sort_data.group(2)
        operation = field_sort_data.group(1)
        if operation == '+':
            operation_factor = pymongo.ASCENDING
        else:
            operation_factor = pymongo.DESCENDING
        return (field, operation_factor)
