from swagger_server.controllers.mongo_db_client import MongoDBClient
from swagger_server.constant import *
import re
from swagger_server.models.update_response import UpdateResponse
from swagger_server.models.update_response import UpdateResponseData
import datetime


class UpdateProvider:
    def __init__(self, namespace, request_data):
        self.client = MongoDBClient()
        self.namespace = namespace
        self.dataKey = request_data.data.data_key
        self.dataValue = request_data.data.data_value

    def update(self):
        update_parameter, update_data = self.build_parameter()
        response = self.execute_db_request(update_parameter, update_data)
        return self.build_response(response)

    def build_response(self, response):
        response_data = UpdateResponse()
        response_data.api_version = "1.0"
        response_data_data = UpdateResponseData()
        response_data_data.matched_count = response.matched_count
        response_data_data.modified_count = response.modified_count
        response_data.data = response_data_data
        return response_data

    def execute_db_request(self, request_parameter, request_data):
        try:
            response = self.client.update(request_parameter, request_data)
        except:
            raise Exception("update data error")
        return response

    def build_parameter(self):
        update_parameter = {}
        update_parameter[OMS_NAMESPACE] = self.namespace
        if self.dataKey.startswith("regex::") and self.dataKey != "regex::":
            try:
                key_pattern = re.match("regex::(.*)", self.dataKey).group(1)
            except:
                raise Exception("Illegal dataKey")
            update_parameter[OMS_DATA_KEY] = re.compile(key_pattern)
        else:
            update_parameter[OMS_DATA_KEY] = self.dataKey
        now_time = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
        update_data = {}
        update_data[OMS_DATA_VALUE] = self.dataValue
        update_data[OMS_UPDATE_TIME] = now_time
        update_body = [{"$set": update_data}]
        return update_parameter, update_body
