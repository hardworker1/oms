from swagger_server.controllers.mongo_db_client import MongoDBClient
from swagger_server.constant import *
import re
from swagger_server.models.delete_reponse import DeleteReponse
from swagger_server.models.delete_response_data import DeleteResponseData

class DeleteProvider:
    def __init__(self, namespace, data_key):
        self.namespace = namespace
        self.data_key = data_key
        self.client = MongoDBClient()

    def delete(self):
        parameter = self.build_parameter()
        delete_count = self.execute_db_request(parameter)
        return self.build_response(delete_count)

    def build_parameter(self):
        delete_parameter = {}
        delete_parameter[OMS_NAMESPACE] = self.namespace
        if self.data_key.startswith("regex::") and self.data_key != "regex::":
            try:
                key_pattern = re.match("regex::(.*)", self.data_key).group(1)
            except:
                raise Exception("Illegal data key")
            delete_parameter[OMS_DATA_KEY] = re.compile(key_pattern)
        else:
            delete_parameter[OMS_DATA_KEY] = self.data_key
        return delete_parameter

    def execute_db_request(self, delete_parameter):
        try:
            delete_count = self.client.delete(delete_parameter).deleted_count
        except:
            raise Exception("delete data error")
        return delete_count

    def build_response(self, delete_count):
        delete_response = DeleteReponse()
        delete_response.api_version = "1.0"
        delete_response_data = DeleteResponseData()
        delete_response_data.delete_count = delete_count
        delete_response.data = delete_response_data
        return delete_response