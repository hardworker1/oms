# coding: utf-8

from __future__ import absolute_import
from datetime import date, datetime  # noqa: F401

from typing import List, Dict  # noqa: F401

from swagger_server.models.base_model_ import Model
from swagger_server import util


class UpdateRequestData(Model):
    """NOTE: This class is auto generated by the swagger code generator program.

    Do not edit the class manually.
    """

    def __init__(self, data_key: str=None, data_value: object=None):  # noqa: E501
        """UpdateRequestData - a model defined in Swagger

        :param data_key: The data_key of this UpdateRequestData.  # noqa: E501
        :type data_key: str
        :param data_value: The data_value of this UpdateRequestData.  # noqa: E501
        :type data_value: object
        """
        self.swagger_types = {
            'data_key': str,
            'data_value': object
        }

        self.attribute_map = {
            'data_key': 'dataKey',
            'data_value': 'dataValue'
        }

        self._data_key = data_key
        self._data_value = data_value

    @classmethod
    def from_dict(cls, dikt) -> 'UpdateRequestData':
        """Returns the dict as a model

        :param dikt: A dict.
        :type: dict
        :return: The update_request_data of this UpdateRequestData.  # noqa: E501
        :rtype: UpdateRequestData
        """
        return util.deserialize_model(dikt, cls)

    @property
    def data_key(self) -> str:
        """Gets the data_key of this UpdateRequestData.


        :return: The data_key of this UpdateRequestData.
        :rtype: str
        """
        return self._data_key

    @data_key.setter
    def data_key(self, data_key: str):
        """Sets the data_key of this UpdateRequestData.


        :param data_key: The data_key of this UpdateRequestData.
        :type data_key: str
        """
        if data_key is None:
            raise ValueError("Invalid value for `data_key`, must not be `None`")  # noqa: E501

        self._data_key = data_key

    @property
    def data_value(self) -> object:
        """Gets the data_value of this UpdateRequestData.


        :return: The data_value of this UpdateRequestData.
        :rtype: object
        """
        return self._data_value

    @data_value.setter
    def data_value(self, data_value: object):
        """Sets the data_value of this UpdateRequestData.


        :param data_value: The data_value of this UpdateRequestData.
        :type data_value: object
        """
        if data_value is None:
            raise ValueError("Invalid value for `data_value`, must not be `None`")  # noqa: E501

        self._data_value = data_value
