# coding: utf-8

from __future__ import absolute_import

from flask import json
from six import BytesIO

from swagger_server.models.create_request import CreateRequest  # noqa: E501
from swagger_server.models.create_response import CreateResponse  # noqa: E501
from swagger_server.models.delete_reponse import DeleteReponse  # noqa: E501
from swagger_server.models.retrieve_response import RetrieveResponse  # noqa: E501
from swagger_server.models.update_request import UpdateRequest  # noqa: E501
from swagger_server.models.update_response import UpdateResponse  # noqa: E501
from swagger_server.test import BaseTestCase


class TestOmsController(BaseTestCase):
    """OmsController integration test stubs"""

    def test_create(self):
        """Test case for create

        
        """
        create_rquest = CreateRequest()
        headers = [('Authorization', 'Authorization_example')]
        response = self.client.open(
            '/oms/v1/item',
            method='POST',
            data=json.dumps(create_rquest),
            headers=headers,
            content_type='application/json')
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))

    def test_delete(self):
        """Test case for delete

        
        """
        query_string = [('namespace', 'namespace_example'),
                        ('dataKey', 'dataKey_example')]
        headers = [('Authorization', 'Authorization_example')]
        response = self.client.open(
            '/oms/v1/items',
            method='DELETE',
            headers=headers,
            content_type='application/json',
            query_string=query_string)
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))

    def test_retrieve(self):
        """Test case for retrieve

        
        """
        query_string = [('namespace', 'namespace_example'),
                        ('dataKey', 'dataKey_example'),
                        ('pageFrom', 56),
                        ('pageSize', 56),
                        ('filedSort', 'filedSort_example'),
                        ('requiredTotalSize', true)]
        headers = [('Authorization', 'Authorization_example')]
        response = self.client.open(
            '/oms/v1/items',
            method='GET',
            headers=headers,
            content_type='application/json',
            query_string=query_string)
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))

    def test_update(self):
        """Test case for update

        
        """
        update_request = UpdateRequest()
        query_string = [('namespace', 'namespace_example')]
        headers = [('Authorization', 'Authorization_example')]
        response = self.client.open(
            '/oms/v1/items',
            method='PATCH',
            data=json.dumps(update_request),
            headers=headers,
            content_type='application/json',
            query_string=query_string)
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))


if __name__ == '__main__':
    import unittest
    unittest.main()
